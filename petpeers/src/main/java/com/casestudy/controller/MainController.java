package com.casestudy.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.casestudy.model.Pet;
import com.casestudy.model.User;
import com.casestudy.service.UserService;
import com.casestudy.validators.LoginValidator;
import com.casestudy.validators.PetValidator;
import com.casestudy.validators.UserValidator;

@Controller
public class MainController {// Main Controller
	@Autowired
	UserService userService;
	@Autowired
	UserValidator userValidator;
	@Autowired
	LoginValidator loginValidator;
	@Autowired
	PetValidator petValidator;
	User user1;

	private static final Logger LOGGER = Logger.getLogger(MainController.class);

	@GetMapping(value = "/")
	public ModelAndView index() {
		LOGGER.info("Successfully enter Index");
		ModelAndView modelAndView = new ModelAndView("loginPage");// default first page login
		modelAndView.addObject("user", new User());
		return modelAndView;
	}

	@RequestMapping(value = "redirectLogPage")
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("loginPage");
		modelAndView.addObject("user", new User());
		return modelAndView;
	}

	@PostMapping(value = "authenticateUser")
	public ModelAndView authenticateUser(HttpServletRequest request, @ModelAttribute("user") User user) {
		LOGGER.info("Authenticating User");
		user1 = userService.authenticateUser(user.getUserName(), user.getUserPassword());
		if (user1 != null) {
			LOGGER.info("Authentic User successfull");
			ModelAndView modelAndView = new ModelAndView("");
			modelAndView.setViewName("redirect:homePage");
			return modelAndView;
		}

		else {
			LOGGER.error("Authentic User failed");
			ModelAndView modelAndView = new ModelAndView("loginPage");
			modelAndView.addObject("Loginerror", "Username or Password error");
			return modelAndView;

		}

	}

	@RequestMapping(value = "redirectRegPage")
	public ModelAndView register() {
		ModelAndView modelAndView = new ModelAndView("registrationPage");
		modelAndView.addObject("user", new User());
		return modelAndView;
	}

	@PostMapping(value = "saveUser")
	public ModelAndView saveuser(@ModelAttribute("user") User user) {
		LOGGER.info("Saving User");
		if(user.getUserName().equals("")||user.getConfirmPassword().equals("")||user.getUserName().equals(""))
		{
			ModelAndView modelAndView = new ModelAndView("registrationPage");
			LOGGER.info("User saved successfully");
			modelAndView.addObject("Error", "All fields are Mandatory");
			return modelAndView;
		}
		
		ModelAndView modelAndView = new ModelAndView("loginPage");
		user = userService.saveUser(user);
		LOGGER.info("User saved successfully");
		modelAndView.addObject("successmessage", "you have Successfully Registered!!!");
		return modelAndView;
	}

	@RequestMapping(value = "redirectHomepage")
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("addPetPage");
		modelAndView.addObject("pet", new Pet());
		return modelAndView;

	}

	@RequestMapping(value = "buyPet")
	public ModelAndView buyPet(HttpServletRequest request) {
		String peti = request.getParameter("petId");
		int petId = Integer.parseInt(peti);
		int userId = (int) user1.getUserId();
		userService.buyPet(userId, petId);// calling buy pet function
		LOGGER.info("Pet buyed successfully ");
		ModelAndView modelAndView = new ModelAndView("");
		modelAndView.setViewName("redirect:homePage");
		return modelAndView;
	}

	@PostMapping(value = "savePet")
	public ModelAndView savePet(@ModelAttribute("pet") Pet pet) {
		LOGGER.info("Saving Pet");
		userService.savePet(pet);
		LOGGER.info("Pet saved successfully");
		ModelAndView modelAndView = new ModelAndView("");
		modelAndView.setViewName("redirect:homePage");
		return modelAndView;
	}

	@RequestMapping(value = "homePage")
	public ModelAndView petList(HttpServletRequest request) {
		List<Pet> pets;
		LOGGER.info("Listing pets");
		pets = userService.getAllPets(); // calling getall pets method
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("pets", pets);
		modelAndView.addObject("user1", user1);
		return modelAndView;
	}

	@RequestMapping(value = "myPets")
	public ModelAndView myPets(HttpServletRequest request) {
		List<Pet> pets;
		LOGGER.info("Listing Mypets");
		int userId = (int) user1.getUserId();
		pets = userService.getMyPets(userId);
		ModelAndView modelAndView = new ModelAndView("myPetsPage");
		modelAndView.addObject("pets", pets);
		return modelAndView;
	}

	@RequestMapping(value = "logout")
	public ModelAndView logout(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.invalidate(); // invalidating the session
		LOGGER.info("Logged out successfully");
		ModelAndView modelandview = new ModelAndView("loginPage");
		modelandview.addObject("user", new User());
		return modelandview;
	}

}