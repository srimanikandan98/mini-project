package com.casestudy.exception;


import org.hibernate.exception.ConstraintViolationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.casestudy.model.User;

@ControllerAdvice
public class CustomExceptionHandler {
	@ExceptionHandler(value = ConstraintViolationException.class)
	public ModelAndView exceptionHandler() {
		ModelAndView modelAndView = new ModelAndView("registrationPage");

		modelAndView.addObject("user", new User());

		modelAndView.addObject("Error", "User Name Alredy Exsist");

		return modelAndView;
	}
}
