package com.casestudy.dao;

import java.util.List;

import com.casestudy.model.Pet;
import com.casestudy.model.User;

public interface UserDAO {
	public abstract User saveUser(User user);

	public abstract User authenticateUser(String userName, String password);


	public abstract Pet savePet(Pet pet);

	public abstract List<Pet> getAllPets();

	public abstract List<Pet> getMyPets(int userId);

	public abstract Pet buyPet(int userId, int petId);
}
