package com.casestudy.validators;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.casestudy.model.User;
import com.casestudy.service.UserService;

@Component
public class UserValidator implements Validator {
	private static final Logger log = Logger.getLogger(UserValidator.class);

	@Autowired
	UserService userService;

	public boolean supports(Class<?> clazz) {

		return User.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		log.info("Validating User");
		ValidationUtils.rejectIfEmpty(errors, "userName", "user.userName.empty");
		ValidationUtils.rejectIfEmpty(errors, "userPassword", "user.userPassword.empty");
		ValidationUtils.rejectIfEmpty(errors, "confirmPassword", "user.confirmPassword.empty");

		User user = (User) target;
		String userName = user.getUserName();
		String password = user.getUserPassword();
		String confirmPassword = user.getConfirmPassword();

		if (userName != "") {
		//	boolean flag = userService.checkUserName(userName);
//			if (flag) {
//			log.error("User Validation:User already exist");
//				errors.rejectValue("userName", "user.userName.exist");
//			}
		}
		if (!(password.equals(confirmPassword))) {
			errors.rejectValue("confirmPassword", "user.confirmPassword.notmatch");
		}

	}

}
