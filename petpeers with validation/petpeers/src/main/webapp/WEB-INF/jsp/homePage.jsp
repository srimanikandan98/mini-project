<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@page import="com.casestudy.model.Pet"%>
<%@page import="java.util.List"%>
<%@page import="com.casestudy.model.User"%>
<%@page import="java.util.Set"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
<style>
#div1 {
	width: 500px;
	height: 500px;
	border-style: double;
	border-color: black;
	border-radius: 6pt;
	background-color: #f5f7f7;
	top: 150px;
	left: 35%;
	position: relative;
}

#header {
	overflow: hidden;
	background-color: white;
}

.navmenu {
	border-radius: 1pt;
	color: #000;
	text-align: center;
	width: 100px;
	height: 60px;
	padding: 20px;
	text-decoration: none;
	font-family: fantasy;
	font-size: 17px;
	font-weight: bold;
}

.navmenu:hover {
	background-color: aqua;
	color: maroon;
}

.navmenu:active {
	background-color: gray;
	color: maroon;
}

.image {
	float: left;
}
</style>
</head>
<body style="background-color: #e8eded; margin: 0;">

	<div class="image">
		<img src="pet-shop-logo-vector-15498684.jpg" alt="logo" height="61"
			width="92" />
	</div>

	<div id="header" style="height: 61px">
		<a style="float: left;" class="navmenu" href="homePage">Home</a> <a
			style="float: left;" class="navmenu" href="myPets">MyPet</a> <a
			style="float: left;" class="navmenu" href="redirectHomepage">AddPet</a>
		<a style="float: right;" class="navmenu" href="logout">LogOut</a>


	</div>
	<div>
		<table style="align: center; border: 1; border-color: black">
			<tr bgcolor="#18b3de">
				<th><b>Pet Id</b></th>
				<th><b>Pet Name</b></th>
				<th><b>Pet Age</b></th>
				<th><b>Pet location</b></th>
				<th><b>Action</b></th>

			</tr>
			<%-- Fetching the attributes of the request object
             which was previously set by the servlet 
              "StudentServlet.java"
        --%>
			<%
				User user1 = (User) request.getAttribute("user1");
				//Pet pet1;
				//pet1.setUser(user1);
			%>



			<%
				List<Pet> pets = (List<Pet>) request.getAttribute("pets");
				for (Pet pet : pets) {
			%>
			<%-- Arranging data in tabular form
        --%>
			<tr>
				<td><%=pet.getPetId()%></td>
				<td><%=pet.getPetName()%></td>
				<td><%=pet.getPetAge()%></td>
				<td><%=pet.getPetPlace()%></td>
				<td><form action="buyPet" method="post">
						<%
							if (pet.getUser() != null) {
						%><input type="button" value="Sold"
							disabled="disabled"></input>
						<%
							} else {
						%><input type="hidden" name="petId"
							value="<%=pet.getPetId()%>"></input><input type="submit"
							value="Buy"></input>
					</form>
					<%
						}
					%></td>



			</tr>
			<%
				}
			%>
		</table>

	</div>

</body>
</html>