<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@page import="com.casestudy.model.Pet"%>
<%@page import="java.util.List"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>MY Pets Page</title>
<style>
#div1{
	width:500px;
	height:500px;
	border-style:double;
	border-color:black;
	border-radius:6pt;
	background-color: #f5f7f7;
	top:150px;
	left:35%;
	position:relative;
	}
	#header{
	overflow:hidden;
	background-color:white;
	}

.navmenu{
		border-radius:1pt;
		color: #000;
		text-align: center;
		width:100px;
		height:60px;
		padding: 20px;
		text-decoration: none;
		font-family:fantasy;
		font-size: 17px;
		font-weight: bold;
	}
	.navmenu:hover {
	background-color: aqua;
	color:maroon;
}
	.navmenu:active{
	background-color: gray;
	color:maroon;

	
}
.image{
float:left;
}

</style>
</head>
<body  style="background-color:#e8eded; margin:0;">

	<div class="image">
	<img src="pet-shop-logo-vector-15498684.jpg" alt="logo" height="61" width="92" />
	</div>
	
	<div id="header" style="height: 61px">
		<a style="float: left;" class="navmenu" href="homePage">Home</a>
		<a style="float: left;" class="navmenu" href="redirectHomepage">AddPet</a>
		<a style="float: right;" class="navmenu" href="logout">LogOut</a>
	
		
	</div> 

<h2>Pet List</h2>
        
        <table style= "align:center; border:1 ; border-color:black">
			<tr>
				<td width="119" align="center"><font size="5sp"><b>#</b></font></td>
				<td width="168" align="center" ><font size="5sp"><b>Pet Name</b></font></td>
				<td width="168" align="center"><font size="5sp"><b>Place</b></font></td>
				<td width="119" align="center"><font size="5sp"><b>Age</b></font></td>
			</tr>
	 <%List<Pet> pets = (List<Pet>)request.getAttribute("pets");
        for(Pet pet:pets){%>
		<tr>
			<td width="119" align="center"><font size="4sp"><%=pet.getPetId()%></font></td>
			<td width="168" align="center"><font size="4sp"><%=pet.getPetName()%></font></td>
			<td width="168" align="center"><font size="4sp"><%=pet.getPetPlace()%></font></td>
			<td width="119" align="center"><font size="4sp"><%=pet.getPetAge()%></font></td>
		</tr>
		<%}%>
		</table>

	
	
	

</body>
</html>
